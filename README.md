# SceneKitLayout

https://www.icloud.com/iclouddrive/009Cuzs47SoybaXJaYgx1CdHw#SwiftUI3D

Create nodes like this:
```
import SceneKitLayout
struct DemoCard: Node {
    var body: some Node {
        ZStack(alignment: .init(x: .right, y: .bottom)) {
            Card(cornerRadius: 20)
            VStack(alignment: .init(x: .right), spacing: 30) {
                Text("JAN MAZURCZAK")
                HStack(spacing: 20) {
                    Text("8888")
                    Text("8888")
                    Text("8888")
                    Text("8888")
                }
            }
            .chamfer(radius: 4)
            .padding(left: 40, right: 40, bottom: 40)
        }
        .frame(width: 400, height: 240, length: 8)
    }
}
```


and then plug them into SwiftUI like so:
```
import SwiftUI
import SceneKitLayout
struct ContentView: View {
    var body: some View {
        LayoutableSceneView(hud: DemoCard())
    }
}
```
