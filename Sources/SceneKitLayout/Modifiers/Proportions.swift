//
//  Proportions.swift
//
//
//  Created by Jan Mazurczak on 23/10/2021.
//

// At this point Proportions struggle handling max size.
// Rethink this modifier.

//import SceneKit
//
//public struct Proportions<Body: Node>: LayoutingNode {
//
//    public init(
//        body: Body,
//        proportions: LayoutSize,
//        alignment: Alignment = .center
//    ) {
//        self.body = body
//        self.layout = .init(proportions: proportions, alignment: alignment)
//    }
//
//    public let body: Body
//    public static var hasStaticBody: Bool { true }
//
//    public let layout: Layout
//    public struct Layout: LayoutDefinition {
//
//        let proportions: LayoutSize
//        let alignment: Alignment
//        //TODO: maxSize needs a proper rethinking here, maybe proportions should actually be part of LayoutableContentNeeeds
//        public func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
//            let needs = needsOverlaying(pieces)
//            let max = needs.maxSize.with(proportions: proportions, fill: false)
//            return .init(
//                requiredSize: needs.requiredSize.with(proportions: proportions, fill: true).limited(to: max),
//                desiredSize: needs.desiredSize.with(proportions: proportions, fill: true).limited(to: max),
//                maxSize: max
//            )
//        }
//
//        public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
//            arrangmentAligning(
//                pieces,
//                in: space.size
//                    .with(proportions: proportions, fill: false)
//                    .aligned(with: alignment, in: space),
//                with: alignment
//            )
//        }
//
//        public func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {}
//
//    }
//
//}
//
//public struct SpaceProportions<Body: Node>: LayoutTransformingNode {
//
//    public init(
//        body: Body,
//        proportions: LayoutSize,
//        alignment: Alignment = .center
//    ) {
//        self.body = body
//        self.transform = .init(proportions: proportions, alignment: alignment)
//    }
//
//    public let body: Body
//    public static var hasStaticBody: Bool { true }
//
//    public let transform: Transform
//    public struct Transform: LayoutTransform {
//
//        let proportions: LayoutSize
//        let alignment: Alignment
//
//        public func space(_ space: LayoutableSpace) -> LayoutableSpace {
//            space
//                .size
//                .with(proportions: proportions, fill: false)
//                .aligned(with: alignment, in: space)
//        }
//
//        public func needs(_ needs: LayoutableContentNeeds) -> LayoutableContentNeeds {
//            needs
//        }
//
//    }
//
//}
//
//extension LayoutSize {
//    func with(proportions: LayoutSize, fill: Bool) -> LayoutSize {
//        let scales = [
//            (x, proportions.x),
//            (y, proportions.y),
//            (z, proportions.z)
//        ]
//        .filter { $0.1 > 0 }
//        .map { $0 / $1 }
//        .sorted(by: <)
//        let scale = fill ? (scales.last ?? 1) : (scales.first ?? 0)
//        return LayoutSize(
//            x: proportions.x * scale,
//            y: proportions.y * scale,
//            z: proportions.z * scale
//        )
//    }
//}
//
//public extension Node {
//    func proportions(
//        _ proportions: LayoutSize,
//        alignment: Alignment = .center
//    ) -> Proportions<Self> {
//        Proportions(
//            body: self,
//            proportions: proportions,
//            alignment: alignment
//        )
//    }
//    func spaceProportions(
//        _ proportions: LayoutSize,
//        alignment: Alignment = .center
//    ) -> SpaceProportions<Self> {
//        SpaceProportions(
//            body: self,
//            proportions: proportions,
//            alignment: alignment
//        )
//    }
//}


