//
//  Rotation.swift
//
//
//  Created by Jan Mazurczak on 02/11/2021.
//

import SceneKit

public struct Changed<Content: Node, Value>: LayoutingNode {
    public init(_ content: Content, at path: WritableKeyPath<SCNNode, Value>, to value: Value) {
        self.layout = .init(path: path, value: value)
        self.body = content
    }
    public let body: Content
    public private(set) var layout: Layout
    public struct Layout: LayoutDefinition {
        let path: WritableKeyPath<SCNNode, Value>
        let value: Value
        public func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
            needsOverlaying(pieces)
        }
        public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
            arrangmentAligning(pieces, in: space, with: .center)
        }
        public func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {
            for node in controlledNodes {
                var node = node
                node[keyPath: path] = value
            }
        }
    }
}

public extension Node {
    func rotate(eulerAngles: SCNVector3) -> some Node {
        Changed(self, at: \.eulerAngles, to: eulerAngles)
    }
    func rotate(_ axis: Axis, _ value: Float) -> some Node {
        Changed(self, at: (\SCNNode.eulerAngles).appending(path: axis.vectorPath), to: value)
    }
}
