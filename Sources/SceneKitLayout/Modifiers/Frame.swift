//
//  Frame.swift
//
//
//  Created by Jan Mazurczak on 22/10/2021.
//

import SceneKit

public struct Frame<Body: Node>: LayoutTransformingNode {
    
    public init(
        body: Body,
        minWidth: Float? = nil,
        idealWidth: Float? = nil,
        maxWidth: Float? = nil,
        minHeight: Float? = nil,
        idealHeight: Float? = nil,
        maxHeight: Float? = nil,
        minLength: Float? = nil,
        idealLength: Float? = nil,
        maxLength: Float? = nil,
        alignment: Alignment = .center
    ) {
        self.body = body
        self.transform = .init(
            minWidth: minWidth,
            idealWidth: idealWidth,
            maxWidth: maxWidth,
            minHeight: minHeight,
            idealHeight: idealHeight,
            maxHeight: maxHeight,
            minLength: minLength,
            idealLength: idealLength,
            maxLength: maxLength,
            alignment: alignment
        )
    }
    
    public let body: Body
    public static var hasStaticBody: Bool { true }
    
    public let transform: Transform
    public struct Transform: LayoutTransform {
        
        let minWidth: Float?
        let idealWidth: Float?
        let maxWidth: Float?
        let minHeight: Float?
        let idealHeight: Float?
        let maxHeight: Float?
        let minLength: Float?
        let idealLength: Float?
        let maxLength: Float?
        let alignment: Alignment
        
        public func space(_ space: LayoutableSpace) -> LayoutableSpace {
            space
        }
        
        public func needs(_ needs: LayoutableContentNeeds) -> LayoutableContentNeeds {
            .init(
                requiredSize: LayoutSize(
                    minWidth ?? needs.requiredSize.x,
                    minHeight ?? needs.requiredSize.y,
                    minLength ?? needs.requiredSize.z
                ),
                desiredSize: LayoutSize(
                    idealWidth ?? needs.desiredSize.x,
                    idealHeight ?? needs.desiredSize.y,
                    idealLength ?? needs.desiredSize.z
                ),
                maxSize: LayoutSize(
                    maxWidth ?? needs.maxSize.x,
                    maxHeight ?? needs.maxSize.y,
                    maxLength ?? needs.maxSize.z
                )
            )
        }
        
    }
    
}

public extension Node {
    func frame(
        width: Float? = nil,
        height: Float? = nil,
        length: Float? = nil,
        alignment: Alignment = .center
    ) -> Frame<Self> {
        Frame(
            body: self,
            minWidth: width,
            idealWidth: width,
            maxWidth: width,
            minHeight: height,
            idealHeight: height,
            maxHeight: height,
            minLength: length,
            idealLength: length,
            maxLength: length,
            alignment: alignment
        )
    }
    func frame(
        minWidth: Float? = nil,
        idealWidth: Float? = nil,
        maxWidth: Float? = nil,
        minHeight: Float? = nil,
        idealHeight: Float? = nil,
        maxHeight: Float? = nil,
        minLength: Float? = nil,
        idealLength: Float? = nil,
        maxLength: Float? = nil,
        alignment: Alignment = .center
    ) -> Frame<Self> {
        Frame(
            body: self,
            minWidth: minWidth,
            idealWidth: idealWidth,
            maxWidth: maxWidth,
            minHeight: minHeight,
            idealHeight: idealHeight,
            maxHeight: maxHeight,
            minLength: minLength,
            idealLength: idealLength,
            maxLength: maxLength,
            alignment: alignment
        )
    }
}

