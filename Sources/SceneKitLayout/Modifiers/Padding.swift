//
//  Padding.swift
//
//
//  Created by Jan Mazurczak on 21/02/2021.
//

import SceneKit

public struct Padding<Body: Node>: LayoutTransformingNode {
    
    public init(body: Body, fromInsets: SCNVector3, toInsets: SCNVector3) {
        self.body = body
        self.transform = .init(
            fromInsets: fromInsets,
            toInsets: toInsets
        )
    }
    
    public let body: Body
    public static var hasStaticBody: Bool { true }
    
    public let transform: Transform
    public struct Transform: LayoutTransform {
        
        let fromInsets: SCNVector3
        let toInsets: SCNVector3
        
        public func space(_ space: LayoutableSpace) -> LayoutableSpace {
            .init(
                from: SCNVector3(
                    space.min.x + fromInsets.x,
                    space.min.y + fromInsets.y,
                    space.min.z + fromInsets.z
                ),
                to: SCNVector3(
                    space.max.x - toInsets.x,
                    space.max.y - toInsets.y,
                    space.max.z - toInsets.z
                )
            )
        }
        
        public func needs(_ needs: LayoutableContentNeeds) -> LayoutableContentNeeds {
            let insets = SCNVector3(
                fromInsets.x + toInsets.x,
                fromInsets.y + toInsets.y,
                fromInsets.z + toInsets.z
            )
            return .init(
                requiredSize: LayoutSize(
                    needs.requiredSize.x + insets.x,
                    needs.requiredSize.y + insets.y,
                    needs.requiredSize.z + insets.z
                ),
                desiredSize: LayoutSize(
                    needs.desiredSize.x + insets.x,
                    needs.desiredSize.y + insets.y,
                    needs.desiredSize.z + insets.z
                ),
                maxSize: LayoutSize(
                    needs.maxSize.x + insets.x,
                    needs.maxSize.y + insets.y,
                    needs.maxSize.z + insets.z
                )
            )
        }
        
    }
    
}

public extension Node {
    func padding(_ padding: Float) -> Padding<Self> {
        Padding(
            body: self,
            fromInsets: SCNVector3(padding, padding, padding),
            toInsets: SCNVector3(padding, padding, padding)
        )
    }
    func padding(_ axis: Axis, _ padding: Float) -> Padding<Self> {
        Padding(
            body: self,
            fromInsets: SCNVector3(0, 0, 0).changing(axis.dimension, to: padding),
            toInsets: SCNVector3(0, 0, 0).changing(axis.dimension, to: padding)
        )
    }
    func padding(
        left: Float = 0, right: Float = 0,
        bottom: Float = 0, top: Float = 0,
        front: Float = 0, back: Float = 0
    ) -> Padding<Self> {
        Padding(
            body: self,
            fromInsets: SCNVector3(left, bottom, back),
            toInsets: SCNVector3(right, top, front)
        )
    }
}
