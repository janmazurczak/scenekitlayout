//
//  Shape.swift
//
//
//  Created by Jan Mazurczak on 21/10/2021.
//

import SceneKit

public struct Shape: SCNNodeRepresentableNode {
    let path: UIBezierPath?
    public init(_ path: UIBezierPath?) {
        self.path = path
    }
    public func create() -> SCNNode {
        let shape = SCNShape(path: path, extrusionDepth: 0)
        return SCNNode(geometry: shape)
    }
    @Environment(\.materials) private var materials
    @Environment(\.chamferRadius) private var chamferRadius
    public func update(node: SCNNode) {
        guard let geometry = node.geometry as? SCNShape else { return }
        geometry.path = path
        geometry.chamferRadius = chamferRadius
        if geometry.materials != materials {
            geometry.materials = materials
        }
    }
    public let layout = GeometryLayout<SCNNode>.zExtrudable { node, space in
        guard let geometry = node.geometry as? SCNShape else { return }
        geometry.extrusionDepth = CGFloat(space.size.z)
    }
}

