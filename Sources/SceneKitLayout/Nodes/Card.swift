//
//  Card.swift
//
//
//  Created by Jan Mazurczak on 22/10/2021.
//

import CoreGraphics

public struct Card: Node {
    let cornerRadius: Float
    public init(cornerRadius: Float = 0) {
        self.cornerRadius = cornerRadius
    }
    public var body: some Node {
        SpaceReader { space in
            Shape(
                .init(
                    roundedRect: CGRect(
                        origin: .zero,
                        size: .init(
                            width: CGFloat(space.x),
                            height: CGFloat(space.y)
                        )
                    ),
                    cornerRadius: CGFloat(cornerRadius)
                )
            )
        }
    }
}

