//
//  Stack.swift
//  
//
//  Created by Jan Mazurczak on 17/09/2021.
//

import SceneKit

public struct Stack<Body: Node>: LayoutingNode {
    
    private let contents: () -> Body
    public let layout: Layout
    
    public init(axis: Axis, alignment: Alignment = .center, spacing: Float = 0, @NodeBuilder contents: @escaping () -> Body) {
        self.contents = contents
        self.layout = .init(axis: axis, spacing: spacing, alignment: alignment)
    }
    
    public var body: Body {
        contents()
    }
    
    public struct Layout: LayoutDefinition {
        
        let axis: Axis
        let spacing: Float
        let alignment: Alignment
        
        private struct Expectations {
            let requiredPieces: [LayoutableContentNeeds]
            let desiredPieces: [LayoutableContentNeeds]
            let requiredSpacing: Float
            let desiredSpacing: Float
            let allSpacing: Float
            let requiredAlongAxis: Float
            let desiredAlongAxis: Float
            let maximumAlongAxis: Float
            init(for pieces: [LayoutableContentNeeds], axis: Axis, spacing: Float) {
                requiredPieces = pieces.filter{ $0.isRequired(alongside: axis) }
                desiredPieces = pieces.filter{ $0.isDesired(alongside: axis) }
                requiredSpacing = Float(max(0, requiredPieces.count - 1)) * spacing
                desiredSpacing = Float(max(0, desiredPieces.count - 1)) * spacing
                allSpacing = Float(max(0, pieces.count - 1)) * spacing
                requiredAlongAxis = requiredPieces.map{ $0.required(alongside: axis) }.reduce(0, +)
                desiredAlongAxis = desiredPieces.map{ max($0.required(alongside: axis), $0.desired(alongside: axis)) }.reduce(0, +)
                maximumAlongAxis = pieces.map{ max(max($0.required(alongside: axis), $0.desired(alongside: axis)), $0.maximum(alongside: axis)) }.reduce(0, +)
            }
        }
        
        public func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
            let known = Expectations(for: pieces, axis: axis, spacing: spacing)
            let required = pieces
                .reduce(SCNVector3.zero) { $1.requiredSize.filling($0) }
                .changing(axis.dimension, to: known.requiredAlongAxis + known.requiredSpacing)
            let desired = pieces
                .reduce(SCNVector3.zero) { $1.desiredSize.filling($0) }
                .changing(axis.dimension, to: known.desiredAlongAxis + known.desiredSpacing)
                .filling(required)
            let maximum = pieces
                .reduce(SCNVector3.zero) { $1.maxSize.filling($0) }
                .changing(axis.dimension, to: known.maximumAlongAxis + known.allSpacing)
                .filling(desired)
            return LayoutableContentNeeds(
                requiredSize: required,
                desiredSize: desired,
                maxSize: maximum
            )
        }
        
        public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
            
            let axisPath = axis.vectorPath
            let availableAlongAxis = max(0, space.size[keyPath: axisPath])
            let known = Expectations(for: pieces, axis: axis, spacing: spacing)
            let origin = space.min[keyPath: axisPath]
            let fullMargin = 0.5 * spacing
            
            let sizedPieces: [SizedPiece]
            
            if known.requiredAlongAxis + known.requiredSpacing >= availableAlongAxis {
                // Not enough space for required elements
                let scaleDown = known.requiredPieces.count > 0
                    ? (availableAlongAxis / (known.requiredAlongAxis + known.requiredSpacing))
                    : 0
                let margin = fullMargin * scaleDown
                sizedPieces = pieces.map { piece in
                    .init(
                        needs: piece,
                        relevant: piece.isRequired(alongside: axis),
                        size: piece.required(alongside: axis) * scaleDown,
                        margin: piece.isRequired(alongside: axis) ? margin : 0
                    )
                }
            } else if known.requiredAlongAxis + known.desiredSpacing >= availableAlongAxis {
                // Not enough to keep spacing between desired elements
                let leftover = availableAlongAxis - (known.requiredAlongAxis + known.requiredSpacing)
                let leftoverPerItem = known.desiredPieces.count > known.requiredPieces.count
                    ? leftover / Float(known.desiredPieces.count - known.requiredPieces.count)
                    : 0
                let leftoverMargin = 0.5 * leftoverPerItem
                sizedPieces = pieces.map { piece in
                    .init(
                        needs: piece,
                        relevant: piece.isRequired(alongside: axis) || piece.isDesired(alongside: axis),
                        size: piece.required(alongside: axis),
                        margin: piece.isRequired(alongside: axis)
                            ? fullMargin
                            : (piece.isDesired(alongside: axis) ? leftoverMargin : 0)
                    )
                }
            } else if known.desiredAlongAxis + known.desiredSpacing >= availableAlongAxis {
                // Not enough for full desired elements
                let leftover = availableAlongAxis - (known.requiredAlongAxis + known.desiredSpacing)
                let extraAmount = known.desiredAlongAxis > known.requiredAlongAxis
                    ? leftover / (known.desiredAlongAxis - known.requiredAlongAxis)
                    : 0
                sizedPieces = pieces.map { piece in
                    let isRelevant = piece.isRequired(alongside: axis) || piece.isDesired(alongside: axis)
                    let extraSize = extraAmount * max(0, piece.desired(alongside: axis) - piece.required(alongside: axis))
                    return .init(
                        needs: piece,
                        relevant: isRelevant,
                        size: piece.required(alongside: axis) + extraSize,
                        margin: isRelevant ? fullMargin : 0
                    )
                }
            } else if known.desiredAlongAxis + known.allSpacing >= availableAlongAxis {
                // Not enough to keep spacing between all elements
                let leftover = availableAlongAxis - (known.desiredAlongAxis + known.desiredSpacing)
                let leftoverPerItem = pieces.count > known.desiredPieces.count
                    ? leftover / Float(pieces.count - known.desiredPieces.count)
                    : 0
                let leftoverMargin = 0.5 * leftoverPerItem
                sizedPieces = pieces.map { piece in
                    .init(
                        needs: piece,
                        relevant: true,
                        size: piece.desired(alongside: axis),
                        margin: (piece.isRequired(alongside: axis) || piece.isDesired(alongside: axis))
                            ? fullMargin
                            : leftoverMargin
                    )
                }
            } else if known.maximumAlongAxis + known.allSpacing >= availableAlongAxis {
                // Not enough to stretch all elements to maximum
                let milestones = pieces // where left space shared between items is changing it's volume
                    .flatMap { piece -> [(Float, Int)] in
                        let maximum = piece.maximum(alongside: axis)
                        let desired = piece.desired(alongside: axis)
                        guard maximum > desired else { return [] }
                        return [(desired, 1), (maximum, -1)]
                    }
                    .sorted { $0.0 < $1.0 }
                var leftover = availableAlongAxis - (known.desiredAlongAxis + known.allSpacing)
                var itemsUsing = 0
                var size: Float = 0
                for milestone in milestones {
                    let used = Float(max(0, itemsUsing)) * (milestone.0 - size)
                    if itemsUsing < 1 || used < leftover {
                        leftover -= used
                        size = milestone.0
                        itemsUsing += milestone.1
                        continue
                    }
                    size = size + (leftover / Float(itemsUsing))
                    break
                }
                sizedPieces = pieces.map { piece in
                    let baseSize = max(piece.required(alongside: axis), piece.desired(alongside: axis))
                    let maxSize = piece.maximum(alongside: axis)
                    return .init(
                        needs: piece,
                        relevant: true,
                        size: max(baseSize, min(size, maxSize)),
                        margin: fullMargin
                    )
                }
            } else {
                // Plenty of space
                sizedPieces = pieces.map { piece in
                    .init(
                        needs: piece,
                        relevant: true,
                        size: piece.maximum(alongside: axis),
                        margin: fullMargin
                    )
                }
            }
            
            return sizedPieces
                    .reduce(PositionedPieces()) { $0.appending($1, origin: origin) }
                    .pieces
                    .map {
                        $0
                            .needs
                            .space(
                                in: space.changing(
                                    axis.dimension,
                                    to: $0.origin, $0.origin + $0.size
                                ),
                                aligning: alignment
                            )
                    }
        }
        
        public func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {}
            
        private struct SizedPiece {
            let needs: LayoutableContentNeeds
            let relevant: Bool
            let size: Float
            let margin: Float
        }
        private struct PositionedPiece {
            let needs: LayoutableContentNeeds
            let origin: Float
            let size: Float
        }
        private struct PositionedPieces {
            var pieces: [PositionedPiece] = []
            var nextOffset: Float?
            func appending(_ piece: SizedPiece, origin: Float) -> Self {
                var result = self
                result.pieces.append(
                    PositionedPiece(
                        needs: piece.needs,
                        origin: (result.nextOffset.map{ $0 + piece.margin } ?? origin),
                        size: piece.size
                    )
                )
                result.nextOffset = piece.relevant
                ? (
                    result.nextOffset.map{ $0 + piece.size + (2 * piece.margin) }
                    ??
                    (origin + piece.size + piece.margin)
                )
                : result.nextOffset
                return result
            }
        }
    }
    
}
