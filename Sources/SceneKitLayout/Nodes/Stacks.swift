//
//  Stacks.swift
//
//
//  Created by Jan Mazurczak on 21/10/2021.
//

public struct ZStack<Body: Node>: Node {
    private let alignment: Alignment
    private let spacing: Float
    private let contents: () -> Body
    public init(alignment: Alignment = .center, spacing: Float = 0, @NodeBuilder contents: @escaping () -> Body) {
        self.alignment = alignment
        self.spacing = spacing
        self.contents = contents
    }
    public var body: some Node {
        Stack(axis: .z, alignment: alignment, spacing: spacing, contents: contents)
    }
}

public struct HStack<Body: Node>: Node {
    private let alignment: Alignment
    private let spacing: Float
    private let contents: () -> Body
    public init(alignment: Alignment = .center, spacing: Float = 0, @NodeBuilder contents: @escaping () -> Body) {
        self.alignment = alignment
        self.spacing = spacing
        self.contents = contents
    }
    public var body: some Node {
        Stack(axis: .x, alignment: alignment, spacing: spacing, contents: contents)
    }
}

public struct VStack<Body: Node>: Node {
    private let alignment: Alignment
    private let spacing: Float
    private let contents: () -> Body
    public init(alignment: Alignment = .center, spacing: Float = 0, @NodeBuilder contents: @escaping () -> Body) {
        self.alignment = alignment
        self.spacing = spacing
        self.contents = contents
    }
    public var body: some Node {
        Stack(axis: .y, alignment: alignment, spacing: spacing, contents: contents)
    }
}
