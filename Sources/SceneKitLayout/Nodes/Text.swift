//
//  Text.swift
//
//
//  Created by Jan Mazurczak on 24/10/2021.
//

import SceneKit

public struct Text: SCNNodeRepresentableNode {
    let text: String
    public init(_ text: String) {
        self.text = text
    }
    public func create() -> SCNNode {
        let text = SCNText()
        text.font = .systemFont(ofSize: 24, weight: .bold)
        return SCNNode(geometry: text)
    }
    @Environment(\.materials) private var materials
    @Environment(\.chamferRadius) private var chamferRadius
    public func update(node: SCNNode) {
        guard let geometry = node.geometry as? SCNText else { return }
        geometry.string = text
        geometry.chamferRadius = chamferRadius
        if geometry.materials != materials {
            geometry.materials = materials
        }
    }
    public let layout = GeometryLayout<SCNNode>.zExtrudable { node, space in
        guard let geometry = node.geometry as? SCNText else { return }
        let size = space.size
        geometry.extrusionDepth = CGFloat(size.z)
        node.position = .init(
            (space.min.x + space.max.x) * 0.5,
            (space.min.y + space.max.y) * 0.5,
            (space.min.z + space.max.z) * 0.5
        )
    }
}


