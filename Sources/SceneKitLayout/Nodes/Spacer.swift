//
//  Spacer.swift
//  
//
//  Created by Jan Mazurczak on 09/05/2021.
//

import SceneKit

public struct Spacer: NodeWithoutBody, LayoutingNode {
    public init() {}
    public let layout = FlexibleLayout()
}

public struct FlexibleLayout: LayoutDefinition {
    
    let alignment: Alignment
    let performLayout: (_ nodes: [SCNNode], _ space: LayoutableSpace) -> Void
    
    public init(alignment: Alignment = .center, layout: @escaping (_ nodes: [SCNNode], _ space: LayoutableSpace) -> Void = { _, _ in }) {
        self.alignment = alignment
        self.performLayout = layout
    }
    
    public func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
        .init(
            requiredSize: .zero,
            desiredSize: .zero,
            maxSize: .greatest
        )
    }
    
    public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
        arrangmentAligning(pieces, in: space, with: alignment)
    }
    
    public func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {
        performLayout(controlledNodes, space)
    }
    
}
