//
//  SpaceReader.swift
//
//
//  Created by Jan Mazurczak on 22/10/2021.
//

import SceneKit

public struct SpaceReader<Content: Node>: LayoutingNode {
    public init(alignment: Alignment = .center, @NodeBuilder content: @escaping (LayoutSize) -> Content) {
        self.layout = .init(alignment: alignment, update: { _ in })
        self.content = content
        layout.update = update(_:)
    }
    private let content: (LayoutSize) -> Content
    @State private var space = LayoutSize.zero
    public var body: some Node {
        content(space)
    }
    private func update(_ spaceSize: LayoutSize) {
        if spaceSize == space { return }
        space = spaceSize
    }
    public private(set) var layout: Layout
    public struct Layout: LayoutDefinition {
        let alignment: Alignment
        var update: (LayoutSize) -> Void
        public func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
            .init(
                requiredSize: .zero,
                desiredSize: .zero,
                maxSize: .greatest
            )
        }
        public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
            arrangmentAligning(pieces, in: space, with: alignment)
        }
        public func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {
            update(space.size)
        }
    }
    
}
