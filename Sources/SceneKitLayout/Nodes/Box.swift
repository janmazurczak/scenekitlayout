//
//  Box.swift
//
//
//  Created by Jan Mazurczak on 21/10/2021.
//

import SceneKit

public struct Box: SCNNodeRepresentableNode {
    public init() {}
    public func create() -> SCNNode {
        let box = SCNBox(width: 0, height: 0, length: 0, chamferRadius: 0)
        return SCNNode(geometry: box)
    }
    @Environment(\.materials) private var materials
    public func update(node: SCNNode) {
        if node.geometry?.materials == materials { return }
        node.geometry?.materials = materials
    }
    public let layout = GeometryLayout<SCNNode>.flexible { node, space in
        guard let geometry = node.geometry as? SCNBox else { return }
        let size = space.size
        geometry.width = CGFloat(size.x)
        geometry.height = CGFloat(size.y)
        geometry.length = CGFloat(size.z)
        node.position = .init(
            (space.min.x + space.max.x) * 0.5,
            (space.min.y + space.max.y) * 0.5,
            (space.min.z + space.max.z) * 0.5
        )
    }
}

