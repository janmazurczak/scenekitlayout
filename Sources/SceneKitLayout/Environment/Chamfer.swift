//
//  Chamfer.swift
//
//
//  Created by Jan Mazurczak on 27/10/2021.
//

import CoreGraphics

public struct ChamferRadiusKey: EnvironmentKey {
    public static var defaultValue: CGFloat { 0 }
}

public extension EnvironmentValues {
    var chamferRadius: CGFloat {
        get { self[ChamferRadiusKey.self] }
        set { self[ChamferRadiusKey.self] = newValue }
    }
}

public extension Node {
    func chamfer(radius: CGFloat) -> some Node {
        environment(\.chamferRadius, radius)
    }
}

