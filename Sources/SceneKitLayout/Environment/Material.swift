//
//  Material.swift
//
//
//  Created by Jan Mazurczak on 24/10/2021.
//

import SceneKit

public struct MaterialsKey: EnvironmentKey {
    public static var defaultValue: [SCNMaterial] { [] }
}

public extension EnvironmentValues {
    var materials: [SCNMaterial] {
        get { self[MaterialsKey.self] }
        set { self[MaterialsKey.self] = newValue }
    }
}

public extension Node {
    func material(_ material: SCNMaterial) -> some Node {
        environment(\.materials, [material])
    }
    func materials(_ materials: [SCNMaterial]) -> some Node {
        environment(\.materials, materials)
    }
}

