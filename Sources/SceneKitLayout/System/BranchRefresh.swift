//
//  BranchRefresh.swift
//  
//
//  Created by Jan Mazurczak on 01/05/2021.
//

import Combine

extension Branch {
    
    func refreshNodeContent() {
        if
            let anchor = scnNode,
            let anchorNode = installedNode as? AnyAnchorNode
        {
            anchorNode.update(anyNode: anchor)
        }
    }
    
    func refreshChildren() {
        let installedNode = self.installedNode
        let oldChildren = Dictionary(
            uniqueKeysWithValues: children.map { ($0.id, $0) }
        )
        let wantedChildren = installedNode.childrenBuilders()
        let wantedChildrenIDs = Set(wantedChildren.map{$0.id})
        
        let newChildren = wantedChildren
            .map {
                ChildBranchCandidate(
                    branch: oldChildren[$0.id],
                    builder: $0
                )
            }
        
        oldChildren
            .filter { !wantedChildrenIDs.contains($0.key) }
            .forEach { $0.value.uninstall() }
        
        newChildren
            .compactMap { $0.building }
            .forEach {
                $0.branch.parent = self
                $0.branch.install()
            }
        
        newChildren
            .compactMap { $0.updating }
            .forEach {
                $0.updateBranch()
            }
        
        children = newChildren.map { $0.branch }
    }
    
    func bind(node: InstalledNode) {
        let properties = Mirror(reflecting: node).children
        var invalidationPublishers = [AnyPublisher<Void, Never>]()
        for property in properties {
            if let stateWrapper = property.value as? AnyState, let name = property.label {
                let initial = stateWrapper.anyInitialValue
                if stateStorage[name] == nil {
                    stateStorage[name] = initial
                }
                stateWrapper.bind { [weak self] in
                    self?.stateStorage[name] ?? initial
                } set: { [weak self] in
                    self?.stateStorage[name] = $0
                }
            }
            if let environmentWrapper = property.value as? AnyEnvironmentValue {
                environmentWrapper.bind(values: environmentValues)
            }
            if let updater = property.value as? Updating {
                invalidationPublishers.append(updater.didUpdate)
            }
        }
        invalidation = Publishers
            .MergeMany(invalidationPublishers)
            .sink { [weak self] in self?.invalidate() }
    }
    
}
