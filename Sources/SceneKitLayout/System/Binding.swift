//
//  Binding.swift
//
//
//  Created by Jan Mazurczak on 22/10/2021.
//

import Combine

@propertyWrapper
public class Binding<Value> {
    private let getter: () -> Value
    private let setter: (Value) -> Void
    internal let willUpdatePublisher = PassthroughSubject<Void, Never>()
    internal let didUpdatePublisher = PassthroughSubject<Void, Never>()
    public init(get: @escaping () -> Value, set: @escaping (Value) -> Void) {
        getter = get
        setter = set
    }
    public var wrappedValue: Value {
        get { getter() }
        set {
            willUpdatePublisher.send()
            setter(newValue)
            didUpdatePublisher.send()
        }
    }
    public var projectedValue: Binding<Value> { self }
}
