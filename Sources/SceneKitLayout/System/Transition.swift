//
//  Transition.swift
//  
//
//  Created by Jan Mazurczak on 03/04/2021.
//

import SceneKit

public struct Transition {
    /// Apply node modifications to reflect invisible state before fade in.
    public let introductionPreparation: (SCNNode) -> Void
    public let fadeIn: SCNAction
    public let fadeOut: SCNAction
}
