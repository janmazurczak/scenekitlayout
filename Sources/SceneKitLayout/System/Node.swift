//
//  Node.swift
//  
//
//  Created by Jan Mazurczak on 01/03/2021.
//

import SceneKit

public protocol Node {
    
    associatedtype Body: Node
    @NodeBuilder var body: Body { get }
    
    // Advanced:
    var layoutSemantics: LayoutSemantics { get }
    static var hasStaticBody: Bool { get }
    func remainsUnchanged(since other: Self) -> Bool
    
    // Framework's internal usage only:
    func children() -> [InternalChildBranchBuilder]
    
}

public extension Node {
    var layoutSemantics: LayoutSemantics { .idle }
    static var hasStaticBody: Bool { false }
    func remainsUnchanged(since other: Self) -> Bool { false }
    func children() -> [InternalChildBranchBuilder] {
        children(\.body, "body")
    }
}

public extension Node where Self: Equatable {
    func remainsUnchanged(since other: Self) -> Bool {
        self == other
    }
}
