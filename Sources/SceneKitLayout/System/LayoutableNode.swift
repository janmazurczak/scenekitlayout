//
//  LayoutableNode.swift
//  
//
//  Created by Jan Mazurczak on 06/04/2021.
//

import SceneKit

internal struct RootContainer<Root: Node>: AnchorNode {
    let body: Root
    func create() -> SCNNode { fatalError("RootContainer should never attempt creating it's own node.") }
    func update(node: SCNNode) {}
    func layout(node: SCNNode, in space: LayoutableSpace) {}
    func remainsUnchanged(since other: Self) -> Bool {
        body.remainsUnchanged(since: other.body)
    }
    let layout = AnchorLayout()
    struct AnchorLayout: AnchorLayoutDefinition {
        func needs(for pieces: [LayoutableContentNeeds], with node: SCNNode) -> LayoutableContentNeeds {
            needsOverlaying(pieces)
        }
        func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
            arrangmentAligning(pieces, in: space)
        }
        func layout(node: SCNNode, in space: LayoutableSpace) {}
    }
}

public class LayoutableNode<Root: Node>: SCNNode {
    let branch: Branch<RootContainer<Root>>
    public init(_ body: Root) {
        branch = Branch(
            id: "root",
            node: RootContainer(body: body),
            layoutSemantics: .layout(AligningLayout(z: .front)),
            incomingLayoutTransforms: [],
            environmentModifiers: []
        )
        super.init()
        branch.scnNode = self
    }
    public func update(with body: Root) {
        branch.update(with: RootContainer(body: body))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func layoutContents(in space: LayoutableSpace) {
        branch.update(availableSpace: space)
    }
}
