//
//  NodeWithoutBody.swift
//  
//
//  Created by Jan Mazurczak on 23/04/2021.
//

public protocol NodeWithoutBody: Node where Body == Never {}
public extension NodeWithoutBody {
    var body: Never {
        fatalError("Call body only on nodes actually exposing the body.")
    }
    func children() -> [InternalChildBranchBuilder] {
        []
    }
}

extension Never: Node {}
