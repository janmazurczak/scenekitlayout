//
//  VoidNode.swift
//  
//
//  Created by Jan Mazurczak on 23/02/2021.
//

import SceneKit

public struct VoidNode: NodeWithoutBody {}
