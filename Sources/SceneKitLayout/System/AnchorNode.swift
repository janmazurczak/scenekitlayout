//
//  AnchorNode.swift
//  
//
//  Created by Jan Mazurczak on 06/03/2021.
//

import SceneKit

public protocol AnyAnchorNode {
    func createAsAny() -> SCNNode
    func update(anyNode: SCNNode)
}

public protocol AnchorNode: LayoutingNode, AnyAnchorNode where AnchorLayout == Layout, AnchorLayout.Anchor == Anchor {
    associatedtype Anchor
    associatedtype AnchorLayout: AnchorLayoutDefinition
    func create() -> Anchor
    func update(node: Anchor)
    var layout: Layout { get }
}

public extension AnchorNode {
    func createAsAny() -> SCNNode {
        create()
    }
    func update(anyNode: SCNNode) {
        guard let node = anyNode as? Anchor else {
            fatalError("Trying to update Anchor with wrong SCNNode subclass.")
        }
        update(node: node)
    }
}

public protocol AnchorLayoutDefinition: LayoutDefinition {
    associatedtype Anchor: SCNNode
    func needs(for pieces: [LayoutableContentNeeds], with node: Anchor) -> LayoutableContentNeeds
    func layout(node: Anchor, in space: LayoutableSpace)
}

public extension AnchorLayoutDefinition {
    func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
        guard let node = controlledNodes.first as? Anchor else {
            fatalError("Trying to read the needs of Node with wrong SCNNode subclass.")
        }
        return needs(for: pieces, with: node)
    }
    func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {
        guard let node = controlledNodes.first as? Anchor else {
            fatalError("Trying to layout Anchor with wrong SCNNode subclass.")
        }
        layout(node: node, in: space)
    }
}
