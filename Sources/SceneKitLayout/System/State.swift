//
//  State.swift
//
//
//  Created by Jan Mazurczak on 22/10/2021.
//

@propertyWrapper
public class State<Value> {
    internal let initialValue: Value
    internal var binder: Binding<Value>?
    public init(wrappedValue: Value) {
        initialValue = wrappedValue
    }
    public var wrappedValue: Value {
        get { projectedValue.wrappedValue }
        set { projectedValue.wrappedValue = newValue }
    }
    public var projectedValue: Binding<Value> {
        guard let binder = binder else {
            fatalError("Accessing State's value before node is installed")
        }
        return binder
    }
}

internal protocol AnyState {
    var anyInitialValue: Any { get }
    func bind(get: @escaping () -> Any, set: @escaping (Any) -> Void)
}

extension State: AnyState {
    var anyInitialValue: Any { initialValue }
    func bind(get: @escaping () -> Any, set: @escaping (Any) -> Void) {
        binder = .init(
            get: {
                guard let value = get() as? Value else {
                    fatalError("State stored is different type than expected")
                }
                return value
            },
            set: { newValue in
                set(newValue)
            }
        )
    }
}

