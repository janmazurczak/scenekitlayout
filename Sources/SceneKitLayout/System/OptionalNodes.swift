//
//  OptionalNodes.swift
//  
//
//  Created by Jan Mazurczak on 07/03/2021.
//

import SceneKit

public enum OptionalNode<Some: Node>: NodeWithoutBody {
    case some(Some)
    case none
    public func children() -> [InternalChildBranchBuilder] {
        switch self {
        case .some: return children(\.some, "some")
        case .none: return []
        }
    }
    private var some: Some {
        switch self {
        case .some(let child): return child
        case .none: fatalError("Trying to retrive non existing child.")
        }
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        switch (self, other) {
        case let (.some(a), .some(b)):
            return a.remainsUnchanged(since: b)
        case (.none, .none):
            return true
        default:
            return false
        }
    }
}

public enum EitherNode<First: Node, Second: Node>: NodeWithoutBody {
    case first(First)
    case second(Second)
    public func children() -> [InternalChildBranchBuilder] {
        switch self {
        case .first: return children(\.first, "first")
        case .second: return children(\.second, "second")
        }
    }
    private var first: First {
        switch self {
        case .first(let child): return child
        case .second: fatalError("Trying to retrive non existing child.")
        }
    }
    private var second: Second {
        switch self {
        case .first: fatalError("Trying to retrive non existing child.")
        case .second(let child): return child
        }
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        switch (self, other) {
        case let (.first(a), .first(b)):
            return a.remainsUnchanged(since: b)
        case let (.second(a), .second(b)):
            return a.remainsUnchanged(since: b)
        default:
            return false
        }
    }
}
