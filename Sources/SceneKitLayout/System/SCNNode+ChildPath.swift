//
//  File.swift
//  
//
//  Created by Jan Mazurczak on 17/04/2021.
//

import SceneKit

public extension SCNNode {
    
    subscript (path: Path) -> SCNNode? {
        if path.components.isEmpty { return self }
        var path = path
        let node = path.components.removeFirst()
        guard
            let child = childNode(
                withName: node.name,
                recursively: node.recursively
            )
        else {
            return nil
        }
        return child[path]
    }
    
    struct Path: ExpressibleByStringLiteral {
        
        var components: [Component]
        public struct Component: ExpressibleByStringLiteral {
            let name: String
            var recursively: Bool
            public init(stringLiteral value: String) {
                name = value
                recursively = false
            }
            public init(_ name: String, recursively: Bool = false) {
                self.name = name
                self.recursively = recursively
            }
        }
        
        /// Provide path in format "nodeA.nodeB.nodeC"
        public init(_ value: String) {
            components = value
                .split(separator: ".")
                .map(String.init)
                .map { Component($0, recursively: false) }
        }
        
        public init(stringLiteral value: String) {
            self.init(value)
        }
        
        public func recursively(_ recursively: Bool = true) -> Path {
            if components.isEmpty { return self }
            var result = self
            for i in 0..<result.components.count {
                result.components[i].recursively = recursively
            }
            return result
        }
        
        public func appending(_ pathComponent: Component) -> Path {
            var result = self
            result.components.append(pathComponent)
            return result
        }
        
    }
    
}

