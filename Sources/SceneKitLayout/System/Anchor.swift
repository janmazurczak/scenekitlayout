//
//  Anchor.swift
//  
//
//  Created by Jan Mazurczak on 22/02/2021.
//

import SceneKit

public struct Anchor<Body: Node>: AnchorNode {
    
    public init(body: Body, anchor: SCNVector3) {
        self.body = body
        self.layout = .init(anchor: anchor)
    }
    
    public let body: Body
    public static var hasStaticBody: Bool { true }
    
    public func create() -> SCNNode { SCNNode() }
    public func update(node: SCNNode) {}
    
    public let layout: AnchorLayout
    public struct AnchorLayout: AnchorLayoutDefinition {
        
        let anchor: SCNVector3
        
        public func needs(for pieces: [LayoutableContentNeeds], with node: SCNNode) -> LayoutableContentNeeds {
            needsOverlaying(pieces)
        }
        
        public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
            let spaceSize = space.size
            let anchoredSpace = LayoutableSpace(
                from: SCNVector3(
                    -anchor.x * spaceSize.x,
                    -anchor.y * spaceSize.y,
                    -anchor.z * spaceSize.z
                ),
                to: SCNVector3(
                    (1 - anchor.x) * spaceSize.x,
                    (1 - anchor.y) * spaceSize.y,
                    (1 - anchor.z) * spaceSize.z
                )
            )
            return arrangmentAligning(pieces, in: anchoredSpace)
        }
        
        public func layout(node: SCNNode, in space: LayoutableSpace) {
            let spaceSize = space.size
            let position = SCNVector3(
                anchor.x * spaceSize.x + space.min.x,
                anchor.y * spaceSize.y + space.min.y,
                anchor.z * spaceSize.z + space.min.z
            )
            node.position = position
        }
        
    }
    
}

public extension Node {
    func anchor(_ anchor: SCNVector3) -> Anchor<Self> {
        Anchor(
            body: self,
            anchor: anchor
        )
    }
}
