//
//  TupleNodes.swift
//  
//
//  Created by Jan Mazurczak on 07/03/2021.
//

public protocol TupleNode: NodeWithoutBody {}

public struct TupleNode2<A: Node, B: Node>: TupleNode {
    let a: A
    let b: B
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b)
    }
}

public struct TupleNode3<A: Node, B: Node, C: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c)
    }
}

public struct TupleNode4<A: Node, B: Node, C: Node, D: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d)
    }
}

public struct TupleNode5<A: Node, B: Node, C: Node, D: Node, E: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    let e: E
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d"),
            children(\.e, "e")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d) &&
        e.remainsUnchanged(since: other.e)
    }
}

public struct TupleNode6<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    let e: E
    let f: F
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d"),
            children(\.e, "e"),
            children(\.f, "f")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d) &&
        e.remainsUnchanged(since: other.e) &&
        f.remainsUnchanged(since: other.f)
    }
}

public struct TupleNode7<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    let e: E
    let f: F
    let g: G
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d"),
            children(\.e, "e"),
            children(\.f, "f"),
            children(\.g, "g")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d) &&
        e.remainsUnchanged(since: other.e) &&
        f.remainsUnchanged(since: other.f) &&
        g.remainsUnchanged(since: other.g)
    }
}

public struct TupleNode8<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node, H: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    let e: E
    let f: F
    let g: G
    let h: H
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d"),
            children(\.e, "e"),
            children(\.f, "f"),
            children(\.g, "g"),
            children(\.h, "h")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d) &&
        e.remainsUnchanged(since: other.e) &&
        f.remainsUnchanged(since: other.f) &&
        g.remainsUnchanged(since: other.g) &&
        h.remainsUnchanged(since: other.h)
    }
}

public struct TupleNode9<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node, H: Node, I: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    let e: E
    let f: F
    let g: G
    let h: H
    let i: I
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d"),
            children(\.e, "e"),
            children(\.f, "f"),
            children(\.g, "g"),
            children(\.h, "h"),
            children(\.i, "i")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d) &&
        e.remainsUnchanged(since: other.e) &&
        f.remainsUnchanged(since: other.f) &&
        g.remainsUnchanged(since: other.g) &&
        h.remainsUnchanged(since: other.h) &&
        i.remainsUnchanged(since: other.i)
    }
}

public struct TupleNode10<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node, H: Node, I: Node, J: Node>: TupleNode {
    let a: A
    let b: B
    let c: C
    let d: D
    let e: E
    let f: F
    let g: G
    let h: H
    let i: I
    let j: J
    public func children() -> [InternalChildBranchBuilder] {
        [
            children(\.a, "a"),
            children(\.b, "b"),
            children(\.c, "c"),
            children(\.d, "d"),
            children(\.e, "e"),
            children(\.f, "f"),
            children(\.g, "g"),
            children(\.h, "h"),
            children(\.i, "i"),
            children(\.j, "j")
        ]
        .flatMap{$0}
    }
    public func remainsUnchanged(since other: Self) -> Bool {
        a.remainsUnchanged(since: other.a) &&
        b.remainsUnchanged(since: other.b) &&
        c.remainsUnchanged(since: other.c) &&
        d.remainsUnchanged(since: other.d) &&
        e.remainsUnchanged(since: other.e) &&
        f.remainsUnchanged(since: other.f) &&
        g.remainsUnchanged(since: other.g) &&
        h.remainsUnchanged(since: other.h) &&
        i.remainsUnchanged(since: other.i) &&
        j.remainsUnchanged(since: other.j)
    }
}
