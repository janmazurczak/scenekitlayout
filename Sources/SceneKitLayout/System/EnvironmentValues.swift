//
//  EnvironmentValues.swift
//
//
//  Created by Jan Mazurczak on 24/10/2021.
//

public protocol EnvironmentKey {
    associatedtype Value
    static var defaultValue: Value { get }
}

public struct EnvironmentValues {
    var values = [String: Any]()
    public subscript<K: EnvironmentKey>(_ key: K.Type) -> K.Value {
        //TODO: check how to properly use Type as a key
        get { (values["\(key)"] as? K.Value) ?? key.defaultValue }
        set { values["\(key)"] = newValue }
    }
    internal func applying(_ modifiers: [EnvironmentModifier]) -> EnvironmentValues {
        var result = self
        for modify in modifiers {
            modify(&result)
        }
        return result
    }
}

