//
//  LayoutDefinition.swift
//  
//
//  Created by Jan Mazurczak on 02/05/2021.
//

import SceneKit

public protocol LayoutDefinition {
    func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds
    func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces
    func layout(controlledNodes: [SCNNode], in space: LayoutableSpace)
}

public typealias ArrangedPieces = [LayoutableSpace]

public protocol LayoutingNode: Node {
    associatedtype Layout: LayoutDefinition
    var layout: Layout { get }
}

public extension LayoutingNode {
    var layoutSemantics: LayoutSemantics {
        .layout(layout)
    }
}

public extension LayoutDefinition {
    func needsOverlaying(_ pieces: [LayoutableContentNeeds]) -> LayoutableContentNeeds {
        let maxSize = pieces
            .reduce(LayoutSize.greatest) {
                $0.limited(to: $1.maxSize)
            }
        let requiredSize = pieces
            .reduce(LayoutSize.zero) {
                $0.filling($1.requiredSize)
            }
        let desiredSize = pieces
            .reduce(LayoutSize.zero) {
                $0.filling($1.desiredSize)
            }
            .limited(to: maxSize)
            .filling(requiredSize)
        return .init(
            requiredSize: requiredSize,
            desiredSize: desiredSize,
            maxSize: maxSize
        )
    }
    
    func arrangmentAligning(
        _ pieces: [LayoutableContentNeeds],
        in space: LayoutableSpace,
        with alignment: Alignment = .center,
        fitDimensions: DimensionSet = .all,
        fillDimensions: DimensionSet = .all
    ) -> ArrangedPieces {
        pieces.map {
            $0.space(
                fit: fitDimensions,
                fill: fillDimensions,
                in: space,
                aligning: alignment
            )
        }
    }
}
