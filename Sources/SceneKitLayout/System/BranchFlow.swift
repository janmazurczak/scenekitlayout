//
//  BranchFlow.swift
//  
//
//  Created by Jan Mazurczak on 01/05/2021.
//

import SceneKit

extension Branch {
    
    var anchor: SCNNode? { scnNode ?? parent?.anchor }
    
    var controlledNodes: [SCNNode] {
        if let node = scnNode {
            return [node]
        } else {
            return children
                .flatMap { $0.controlledNodes }
        }
    }
    
    func install() {
        if let anchorNode = installedNode as? AnyAnchorNode {
            let anchor = anchorNode.createAsAny()
            parent?.anchor?.addChildNode(anchor)
            self.scnNode = anchor
        }
        recalculateEnvironmentValues()
    }
    
    func uninstall() {
        parent = nil
        controlledNodes.forEach {
            $0.removeFromParentNode()
        }
        stateStorage = [:]
    }
    
    func recalculateEnvironmentValues() {
        environmentValues = (parent?.environmentValues ?? .init())
            .applying(environmentModifiers)
    }
    
    func invalidate() {
        needsRefresh = true
        parent?.invalidateDescendants()
    }
    
    func update(with node: InstalledNode, invalidateTree: Bool = true) {
        bind(node: node)
        let changed = !node.remainsUnchanged(since: installedNode)
        installedNode = node
        if changed {
            needsRefresh = true
        }
        if changed && invalidateTree {
            parent?.invalidateDescendants()
        }
    }
    
    func invalidateDescendants() {
        if needsDescendantsUpdate { return }
        needsDescendantsUpdate = true
        parent?.invalidateDescendants()
    }
    
}
