//
//  LayoutTransform.swift
//  
//
//  Created by Jan Mazurczak on 01/05/2021.
//

public protocol LayoutTransform {
    func space(_ space: LayoutableSpace) -> LayoutableSpace
    func needs(_ needs: LayoutableContentNeeds) -> LayoutableContentNeeds
}

public protocol LayoutTransformingNode: Node {
    associatedtype Transform: LayoutTransform
    var transform: Transform { get }
}

public extension LayoutTransformingNode {
    var layoutSemantics: LayoutSemantics {
        .transform(transform)
    }
}
