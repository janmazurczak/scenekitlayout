//
//  Branch.swift
//  
//
//  Created by Jan Mazurczak on 05/03/2021.
//

import SceneKit
import Combine

internal protocol AnyBranch: AnyObject {
    /// Reference path from parent
    var id: String { get }
    var parent: AnyBranch? { get set }
    var anchor: SCNNode? { get }
    var controlledNodes: [SCNNode] { get }
    var availableSpace: LayoutableSpace? { get }
    func update(availableSpace space: LayoutableSpace)
    func layoutNeeds(using layoutDefinition: LayoutDefinition) -> LayoutableContentNeeds
    func layoutablePieces(transforms: [LayoutTransform]) -> [LayoutablePiece]
    var environmentModifiers: [EnvironmentModifier] { get set }
    var environmentValues: EnvironmentValues { get set }
    func recalculateEnvironmentValues()
    var incomingLayoutTransforms: [LayoutTransform] { get set }
    func install()
    func performUpdate()
    func uninstall()
    func invalidateDescendants()
}

internal class Branch<InstalledNode: Node>: AnyBranch {
    
    let id: String
    var installedNode: InstalledNode
    var layoutSemantics: LayoutSemantics
    var incomingLayoutTransforms: [LayoutTransform]
    
    weak var scnNode: SCNNode?
    
    var availableSpace: LayoutableSpace? {
        didSet {
            invalidateDescendants()
        }
    }
    
    var environmentValues: EnvironmentValues = .init()
    var environmentModifiers: [EnvironmentModifier] {
        didSet {
            recalculateEnvironmentValues()
        }
    }
    
    var needsRefresh: Bool = true
    var needsLayout: Bool = false
    var needsDescendantsUpdate: Bool = false
    
    var stateStorage = [String: Any]()
    var invalidation: AnyCancellable?
    
    weak var parent: AnyBranch?
    var children = [AnyBranch]()
    
    init(
        id: String,
        node: InstalledNode,
        layoutSemantics: LayoutSemantics,
        incomingLayoutTransforms: [LayoutTransform],
        environmentModifiers: [EnvironmentModifier]
    ) {
        self.id = id
        self.installedNode = node
        self.layoutSemantics = layoutSemantics
        self.incomingLayoutTransforms = incomingLayoutTransforms
        self.environmentModifiers = environmentModifiers
        bind(node: node)
        recalculateEnvironmentValues()
    }
    
    func performUpdate() {
        if needsRefresh {
            needsRefresh = false
            needsLayout = true
            refreshChildren()
            refreshNodeContent()
            performChildrenUpdate()
        }
        if needsDescendantsUpdate {
            needsLayout = true
            performChildrenUpdate()
        }
        if
            needsLayout,
            let space = availableSpace,
            let layout = layoutSemantics.layout
        {
            needsLayout = false
            layoutPieces(in: space, using: layout)
            layoutControlledNodes(in: space, using: layout)
            performChildrenUpdate()
        }
    }
    
    func performChildrenUpdate() {
        needsDescendantsUpdate = false
        children.forEach { $0.performUpdate() }
    }
    
}
