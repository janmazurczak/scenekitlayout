//
//  EnvironmentModifier.swift
//
//
//  Created by Jan Mazurczak on 24/10/2021.
//

internal typealias EnvironmentModifier = (inout EnvironmentValues) -> Void

internal protocol AnyEnvironmentModifierNode {
    var modifier: EnvironmentModifier { get }
}

internal struct EnvironmentModifierNode<Content: Node>: Node, AnyEnvironmentModifierNode {
    let body: Content
    let modifier: EnvironmentModifier
    static var hasStaticBody: Bool { true }
}

public extension Node {
    func environment<Value>(_ path: WritableKeyPath<EnvironmentValues, Value> , _ value: Value) -> some Node {
        EnvironmentModifierNode(body: self) {
            $0[keyPath: path] = value
        }
    }
}
