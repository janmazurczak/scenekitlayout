//
//  Environment.swift
//
//
//  Created by Jan Mazurczak on 24/10/2021.
//

import Combine

@propertyWrapper
public class Environment<Value> {
    let path: KeyPath<EnvironmentValues, Value>
    var values: EnvironmentValues = EnvironmentValues()
    //TODO: feed with changes:
    internal let didUpdatePublisher = PassthroughSubject<Void, Never>()
    public init(_ path: KeyPath<EnvironmentValues, Value>) {
        self.path = path
    }
    public var wrappedValue: Value {
        values[keyPath: path]
    }
}

internal protocol AnyEnvironmentValue {
    func bind(values: EnvironmentValues)
}

extension Environment: AnyEnvironmentValue {
    func bind(values: EnvironmentValues) {
        self.values = values
    }
}

