//
//  SceneManager.swift
//  
//
//  Created by Jan Mazurczak on 11/04/2021.
//

import SceneKit

internal class SceneManager<HUD: Node>: NSObject, ObservableObject, SCNSceneRendererDelegate {
    
    var setup: SceneSetup = DefaultSceneSetup()
    var hud: HUD? {
        didSet {
            guard let hud = hud else { return }
            hudNode.update(with: hud)
        }
    }
    
    private(set) lazy var hudNode: LayoutableNode<HUD> = buildHudNode()
    private let hudNodePathComponent: SCNNode.Path.Component = "LayoutableHUDNode"
    private func buildHudNode() -> LayoutableNode<HUD> {
        guard let hud = hud else {
            fatalError("Scene manager is trying to build hud node without knowing the hud yet.")
        }
        let hudNode = LayoutableNode(hud)
        hudNode.name = hudNodePathComponent.name
        return hudNode
    }
    
    private func cameraNode(in scene: SCNScene) -> SCNNode {
        guard let cameraNode = scene.rootNode[setup.cameraNodePath] else {
            fatalError("SceneSetup returns cameraNodePath that is failing to point at camera node correctly in scene returned by createScene()")
        }
        return cameraNode
    }
    
    var cameraNode: SCNNode {
        cameraNode(in: scene)
    }
    
    var camera: SCNCamera {
        guard let camera = cameraNode.camera else {
            fatalError("SceneSetup cameraNodePath points at a node that is not carrying any SCNCamera.")
        }
        return camera
    }
    
    private(set) lazy var scene: SCNScene = buildScene()
    private func buildScene() -> SCNScene {
        let scene = setup.createScene()
        let cameraNode = self.cameraNode(in: scene)
        if hudNode.parent != cameraNode {
            if hudNode.parent != nil {
                hudNode.removeFromParentNode()
            }
            cameraNode.addChildNode(hudNode)
        }
        return scene
    }
    
    private var requestedHudSpaceUpdate: (size: CGSize, insets: UIEdgeInsets)?
    func requestHudSpaceUpdate(with size: CGSize, and insets: UIEdgeInsets) {
        requestedHudSpaceUpdate = (size: size, insets: insets)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if let (size, insets) = requestedHudSpaceUpdate {
            requestedHudSpaceUpdate = nil
            layoutHud(in: size, with: insets)
        }
        hudNode.branch.performUpdate()
    }
    
    private func layoutHud(in size: CGSize, with insets: UIEdgeInsets) {
        let camera = self.camera
        let fullSize = CGSize(
            width: size.width + insets.left + insets.right,
            height: size.height + insets.top + insets.bottom
        )
        let fovScale = 2 * tan(Float(camera.fieldOfView) * 0.5 * .pi / 180)
        let fov = (camera.projectionDirection == .some(.horizontal))
            ? fullSize.width
            : fullSize.height
        let scale = (fovScale * setup.hudDistance) / Float(fov)
        let depth = setup.hudDepth.depth(in: size)
        hudNode.position.z = -0.5 * Float(depth) * scale - setup.hudDistance
        hudNode.scale = SCNVector3(scale, scale, scale)
        hudNode.layoutContents(
            in: LayoutableSpace(
                from: SCNVector3(
                    -0.5 * fullSize.width + insets.left,
                    -0.5 * fullSize.height + insets.bottom,
                    -0.5 * depth
                ),
                to: SCNVector3(
                    0.5 * fullSize.width - insets.right,
                    0.5 * fullSize.height - insets.top,
                    0.5 * depth
                )
            )
        )
    }
    
}

