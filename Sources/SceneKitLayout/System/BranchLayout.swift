//
//  BranchLayout.swift
//  
//
//  Created by Jan Mazurczak on 01/05/2021.
//

extension Branch {
    
    func update(availableSpace space: LayoutableSpace) {
        availableSpace = space
    }
    
    func layoutControlledNodes(in space: LayoutableSpace, using layoutDefinition: LayoutDefinition) {
        layoutDefinition
            .layout(
                controlledNodes: controlledNodes,
                in: space
            )
    }
    
    func descendingLayoutablePieces(transforms: [LayoutTransform]) -> [LayoutablePiece] {
        children
            .flatMap { $0.layoutablePieces(transforms: transforms) }
    }
    
    func layoutablePieces(transforms: [LayoutTransform]) -> [LayoutablePiece] {
        let allTransforms = transforms + incomingLayoutTransforms
        switch layoutSemantics {
        case .idle:
            return descendingLayoutablePieces(
                transforms: allTransforms
            )
        case .transform(let transform):
            return descendingLayoutablePieces(
                transforms: allTransforms + [transform].compactMap{$0}
            )
        case .layout(let layout):
            return [
                .init(
                    transforms: allTransforms,
                    branch: self,
                    layout: layout
                )
            ]
        }
    }
    
    func layoutNeeds(using layoutDefinition: LayoutDefinition) -> LayoutableContentNeeds {
        let pieces = descendingLayoutablePieces(transforms: [])
        return layoutDefinition
            .needs(
                for: pieces.map(\.needs),
                controlledNodes: controlledNodes
            )
    }
    
    func layoutPieces(in space: LayoutableSpace, using layoutDefinition: LayoutDefinition) {
        let pieces = descendingLayoutablePieces(transforms: [])
        let arrangment = layoutDefinition.arrangment(
            for: pieces.map(\.needs),
            in: space
        )
        guard pieces.count == arrangment.count else {
            fatalError("arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces MUST return exactly the same number of elements as pieces argument contains.")
        }
        if pieces.isEmpty { return }
        for i in 0..<pieces.count {
            let transforms = pieces[i].transforms
            pieces[i].branch.update(availableSpace: arrangment[i].applying(transforms))
        }
    }
    
}

internal struct LayoutablePiece {
    let transforms: [LayoutTransform]
    let branch: AnyBranch
    let layout: LayoutDefinition
    var needs: LayoutableContentNeeds {
        branch.layoutNeeds(using: layout).applying(transforms)
    }
}

internal extension LayoutableSpace {
    func applying(_ transforms: [LayoutTransform]) -> LayoutableSpace {
        transforms.reduce(self) {
            $1.space($0)
        }
    }
}

internal extension LayoutableContentNeeds {
    func applying(_ transforms: [LayoutTransform]) -> LayoutableContentNeeds {
        transforms.reversed().reduce(self) {
            $1.needs($0)
        }
    }
}
