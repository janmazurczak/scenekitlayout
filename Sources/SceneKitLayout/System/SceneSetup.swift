//
//  SceneSetup.swift
//  
//
//  Created by Jan Mazurczak on 17/04/2021.
//

import SceneKit
import SwiftUI

public protocol SceneSetup {
    var cameraNodePath: SCNNode.Path { get }
    var hudDistance: Float { get }
    var hudDepth: SceneSetupHudDepth { get }
    var options: SceneView.Options { get }
    var preferredFramesPerSecond: Int { get }
    var antialiasingMode: SCNAntialiasingMode { get }
    var technique: SCNTechnique? { get }
    func createScene() -> SCNScene
}

public enum SceneSetupHudDepth {
    case fixed(CGFloat)
    case relativeToWidth(CGFloat)
    case relativeToHeight(CGFloat)
    case relativeToShorterEdge(CGFloat)
    case relativeToLongerEdge(CGFloat)
    public func depth(in size: CGSize) -> CGFloat {
        switch self {
        case .fixed(let depth):
            return depth
        case .relativeToWidth(let multiplier):
            return size.width * multiplier
        case .relativeToHeight(let multiplier):
            return size.height * multiplier
        case .relativeToShorterEdge(let multiplier):
            return min(size.width, size.height) * multiplier
        case .relativeToLongerEdge(let multiplier):
            return max(size.width, size.height) * multiplier
        }
    }
}

public struct DefaultSceneSetup: SceneSetup {
    
    public init(
        hudDistance: Float = 1,
        hudDepth: SceneSetupHudDepth = .relativeToShorterEdge(1),
        options: SceneView.Options = [.autoenablesDefaultLighting, .rendersContinuously, .allowsCameraControl],
        preferredFramesPerSecond: Int = 60,
        antialiasingMode: SCNAntialiasingMode = .none,
        technique: SCNTechnique? = nil
    ) {
        self.hudDistance = hudDistance
        self.hudDepth = hudDepth
        self.options = options
        self.preferredFramesPerSecond = preferredFramesPerSecond
        self.antialiasingMode = antialiasingMode
        self.technique = technique
    }
    
    public let cameraNodePath: SCNNode.Path = "DefaultSceneCameraNode"
    public let hudDistance: Float
    public let hudDepth: SceneSetupHudDepth
    public let options: SceneView.Options
    public let preferredFramesPerSecond: Int
    public let antialiasingMode: SCNAntialiasingMode
    public let technique: SCNTechnique?
    
    public func createScene() -> SCNScene {
        let scene = SCNScene()
        
        let cameraNode = SCNNode()
        cameraNode.name = cameraNodePath.components.last?.name
        cameraNode.camera = SCNCamera()
        cameraNode.camera?.zNear = 0.001
        cameraNode.camera?.zFar = 10000
        cameraNode.camera?.fieldOfView = 60
        cameraNode.camera?.projectionDirection = .vertical
        
        scene.rootNode.addChildNode(cameraNode)
        return scene
    }
    
}
