import Combine

internal protocol Updating {
    var didUpdate: AnyPublisher<Void, Never> { get }
}

extension Binding: Updating {
    var didUpdate: AnyPublisher<Void, Never> {
        didUpdatePublisher.eraseToAnyPublisher()
    }
}

extension State: Updating {
    var didUpdate: AnyPublisher<Void, Never> {
        projectedValue.didUpdate.eraseToAnyPublisher()
    }
}

extension Environment: Updating {
    var didUpdate: AnyPublisher<Void, Never> {
        didUpdatePublisher.eraseToAnyPublisher()
    }
}
