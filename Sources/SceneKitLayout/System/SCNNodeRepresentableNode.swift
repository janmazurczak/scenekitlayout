//
//  SCNNodeRepresentableNode.swift
//  
//
//  Created by Jan Mazurczak on 23/02/2021.
//

import SceneKit

public protocol SCNNodeRepresentableNode: AnchorNode, NodeWithoutBody {}

public struct GeometryLayout<Anchor: SCNNode>: AnchorLayoutDefinition {
    
    let extrudableDimension: DimensionSet
    let performLayout: (_ node: Anchor, _ space: LayoutableSpace) -> Void
    
    public init(
        extruding: DimensionSet,
        _ layout: @escaping (_ node: Anchor, _ space: LayoutableSpace) -> Void
    ) {
        extrudableDimension = extruding
        performLayout = layout
    }
    
    public static var fit: Self { Self(extruding: .none) { _, _ in } }
    public static func flexible(
        layout: @escaping (_ node: Anchor, _ space: LayoutableSpace) -> Void
    ) -> Self {
        Self(extruding: .all, layout)
    }
    public static func zExtrudable(
        layout: @escaping (_ node: Anchor, _ space: LayoutableSpace) -> Void
    ) -> Self {
        Self(extruding: .z, layout)
    }
    
    public func needs(for pieces: [LayoutableContentNeeds], with node: Anchor) -> LayoutableContentNeeds {
        let box = node.boundingBox
        let size = LayoutSize(
            box.max.x - box.min.x,
            box.max.y - box.min.y,
            box.max.z - box.min.z
        )
        return .init(
            requiredSize: size.changing(extrudableDimension, to: 0),
            desiredSize: size.changing(extrudableDimension, to: 0),
            maxSize: size.changing(extrudableDimension, to: .greatestFiniteMagnitude)
        )
    }
    
    public func arrangment(for pieces: [LayoutableContentNeeds], in space: LayoutableSpace) -> ArrangedPieces {
        arrangmentAligning(pieces, in: space)
    }
    
    public func layout(node: Anchor, in space: LayoutableSpace) {
        performLayout(node, space)
        let box = node.boundingBox
        let size = LayoutSize(
            box.max.x - box.min.x,
            box.max.y - box.min.y,
            box.max.z - box.min.z
        )
        let space = size.aligned(
            with: .center,
            in: space
        )
        node.position = .init(
            space.min.x - box.min.x,
            space.min.y - box.min.y,
            space.min.z - box.min.z
        )
    }
    
}
