//
//  LayoutableNode.swift
//
//
//  Created by Jan Mazurczak on 14/02/2021.
//

import SceneKit

public struct LayoutableContentNeeds: Equatable {
    public init(requiredSize: LayoutSize, desiredSize: LayoutSize, maxSize: LayoutSize) {
        self.requiredSize = requiredSize
        self.desiredSize = desiredSize
        self.maxSize = maxSize
    }
    public let requiredSize: LayoutSize
    public let desiredSize: LayoutSize
    public let maxSize: LayoutSize
}

public struct LayoutableSpace: Equatable {
    public let min: SCNVector3
    public let max: SCNVector3
    public init(from a: SCNVector3, to b: SCNVector3) {
        min = .init(Swift.min(a.x, b.x), Swift.min(a.y, b.y), Swift.min(a.z, b.z))
        max = .init(Swift.max(a.x, b.x), Swift.max(a.y, b.y), Swift.max(a.z, b.z))
    }
    public var size: LayoutSize {
        .init(max.x - min.x, max.y - min.y, max.z - min.z)
    }
    public func changing(_ dimensions: DimensionSet, to a: Float, _ b: Float) -> LayoutableSpace {
        .init(
            from: min.changing(dimensions, to: a),
            to: max.changing(dimensions, to: b)
        )
    }
}

public typealias LayoutSize = SCNVector3

public extension LayoutSize {
    static let zero = LayoutSize(0, 0, 0)
    static let greatest = LayoutSize(Float.greatestFiniteMagnitude, Float.greatestFiniteMagnitude, Float.greatestFiniteMagnitude)
    func limited(to other: LayoutSize, in dimensions: DimensionSet = .all) -> LayoutSize {
        .init(
            dimensions.x ? min(x, other.x) : x,
            dimensions.y ? min(y, other.y) : y,
            dimensions.z ? min(z, other.z) : z
        )
    }
    func filling(_ other: LayoutSize, in dimensions: DimensionSet = .all) -> LayoutSize {
        .init(
            dimensions.x ? max(x, other.x) : x,
            dimensions.y ? max(y, other.y) : y,
            dimensions.z ? max(z, other.z) : z
        )
    }
    func changing(_ dimensions: DimensionSet, to a: Float) -> LayoutSize {
        .init(
            dimensions.x ? a : x,
            dimensions.y ? a : y,
            dimensions.z ? a : z
        )
    }
}

public extension LayoutableContentNeeds {
    func size(
        in space: LayoutableSpace,
        fit fitDimensions: DimensionSet = .all,
        fill fillDimensions: DimensionSet = .all
    ) -> LayoutSize {
        desiredSize
            .limited(to: space.size, in: fitDimensions)
            .filling(requiredSize)
            .filling(
                space.size.limited(to: maxSize),
                in: fillDimensions
            )
    }
    func space(
        fit fitDimensions: DimensionSet = .all,
        fill fillDimensions: DimensionSet = .all,
        in space: LayoutableSpace,
        aligning alignment: Alignment = .center
    ) -> LayoutableSpace {
        self
            .size(
                in: space,
                fit: .all,
                fill: .all
            )
            .aligned(
                with: alignment,
                in: space
            )
    }
}

public struct DimensionSet {
    public init(x: Bool, y: Bool, z: Bool) {
        self.x = x
        self.y = y
        self.z = z
    }
    public let x: Bool
    public let y: Bool
    public let z: Bool
    public static let none = DimensionSet(x: false, y: false, z: false)
    public static let all = DimensionSet(x: true, y: true, z: true)
    public static let x = DimensionSet(x: true, y: false, z: false)
    public static let y = DimensionSet(x: false, y: true, z: false)
    public static let z = DimensionSet(x: false, y: false, z: true)
}

public enum Axis {
    case x, y, z
    public var vectorPath: WritableKeyPath<SCNVector3, Float> {
        switch self {
        case .x: return \.x
        case .y: return \.y
        case .z: return \.z
        }
    }
    public var dimension: DimensionSet {
        switch self {
        case .x: return .x
        case .y: return .y
        case .z: return .z
        }
    }
}

public extension LayoutableContentNeeds {
    func required(alongside axis: Axis) -> Float {
        requiredSize[keyPath: axis.vectorPath]
    }
    func desired(alongside axis: Axis) -> Float {
        desiredSize[keyPath: axis.vectorPath]
    }
    func maximum(alongside axis: Axis) -> Float {
        maxSize[keyPath: axis.vectorPath]
    }
    func isRequired(alongside axis: Axis) -> Bool {
        required(alongside: axis) >= .leastNormalMagnitude
    }
    func isDesired(alongside axis: Axis) -> Bool {
        desired(alongside: axis) >= .leastNormalMagnitude
    }
}

extension SCNVector3: Equatable {
    public static func == (lhs: SCNVector3, rhs: SCNVector3) -> Bool {
        lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z
    }
}
