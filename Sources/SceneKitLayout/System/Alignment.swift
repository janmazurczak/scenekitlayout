//
//  AlignLayoutDefinition.swift
//  
//
//  Created by Jan Mazurczak on 07/05/2021.
//

import SceneKit

public struct Alignment {
    public init(
        x: Alignment.X = .center,
        y: Alignment.Y = .center,
        z: Alignment.Z = .center
    ) {
        self.x = x
        self.y = y
        self.z = z
    }
    public let x: X
    public let y: Y
    public let z: Z
    public enum X: Float {
        case left = 0
        case center = 0.5
        case right = 1
    }
    public enum Y: Float {
        case top = 1
        case center = 0.5
        case bottom = 0
    }
    public enum Z: Float {
        case back = 0
        case center = 0.5
        case front = 1
    }
    public static let center = Alignment()
    public static let front = Alignment(z: .front)
    public static let back = Alignment(z: .back)
}

public struct AligningLayout: LayoutDefinition {
    
    public let alignment: Alignment
    public let fitDimensions: DimensionSet
    public let fillDimensions: DimensionSet
    
    public init(x: Alignment.X = .center, y: Alignment.Y = .center, z: Alignment.Z = .center, fit: DimensionSet = .all, fill: DimensionSet = .all) {
        alignment = .init(x: x, y: y, z: z)
        fitDimensions = fit
        fillDimensions = fill
    }
    
    public func needs(for pieces: [LayoutableContentNeeds], controlledNodes: [SCNNode]) -> LayoutableContentNeeds {
        needsOverlaying(pieces)
    }
    
    public func arrangment(
        for pieces: [LayoutableContentNeeds],
        in space: LayoutableSpace
    ) -> ArrangedPieces {
        arrangmentAligning(
            pieces,
            in: space,
            with: alignment,
            fitDimensions: fitDimensions,
            fillDimensions: fillDimensions
        )
    }
    
    public func layout(controlledNodes: [SCNNode], in space: LayoutableSpace) {
        // Do nothing with actual SCNNodes
    }
    
}

public struct Aligning<Body: Node>: LayoutingNode {
    public init(body: Body, x: Alignment.X = .center, y: Alignment.Y = .center, z: Alignment.Z = .center, fill: DimensionSet = .none) {
        self.body = body
        self.layout = .init(x: x, y: y, z: z, fill: fill)
    }
    public let body: Body
    public static var hasStaticBody: Bool { true }
    public let layout: AligningLayout
}

public extension Node {
    func align(
        x: Alignment.X = .center, y: Alignment.Y = .center, z: Alignment.Z = .center, fill: DimensionSet = .all
    ) -> Aligning<Self> {
        Aligning(
            body: self,
            x: x, y: y, z: z, fill: fill
        )
    }
}

public extension LayoutSize {
    func aligned(with alignment: Alignment, in space: LayoutableSpace) -> LayoutableSpace {
        let spaceSize = space.size
        let origin = SCNVector3(
            (spaceSize.x - x) * alignment.x.rawValue + space.min.x,
            (spaceSize.y - y) * alignment.y.rawValue + space.min.y,
            (spaceSize.z - z) * alignment.z.rawValue + space.min.z
        )
        return LayoutableSpace(
            from: origin,
            to: .init(
                origin.x + x,
                origin.y + y,
                origin.z + z
            )
        )
    }
}
