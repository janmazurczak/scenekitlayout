//
//  LayoutableSceneView.swift
//
//
//  Created by Jan Mazurczak on 15/01/2021.
//

import SwiftUI
import SceneKit

public struct LayoutableSceneView<HUD: Node>: View {
    
    @StateObject var sceneManager = SceneManager<HUD>()
    @SwiftUI.Environment(\.layoutDirection) var layoutDirection
    
    let hud: HUD
    let setup: SceneSetup
    
    public init(hud: HUD, setup: SceneSetup = DefaultSceneSetup()) {
        self.hud = hud
        self.setup = setup
    }
    
    public var body: some View {
        GeometryReader { g in
            sceneManager.sceneView(
                hud: hud,
                setup: setup,
                size: g.size,
                insets: .init(g.safeAreaInsets, layoutDirection)
            )
            .ignoresSafeArea()
        }
        //.gesture(drag)
    }
    
    var drag: some Gesture {
        DragGesture(minimumDistance: 0)
        //TODO:
//            .onChanged { value in
//                <#code#>
//            }
    }
}

extension SceneManager {
    func sceneView(
        hud: HUD,
        setup: SceneSetup,
        size: CGSize,
        insets: UIEdgeInsets
    ) -> SceneView {
        self.setup = setup
        self.hud = hud
        requestHudSpaceUpdate(
            with: size,
            and: insets
        )
        return SceneView(
            scene: scene,
            pointOfView: cameraNode,
            options: setup.options,
            preferredFramesPerSecond: setup.preferredFramesPerSecond,
            antialiasingMode: setup.antialiasingMode,
            delegate: self,
            technique: setup.technique
        )
    }
}

extension UIEdgeInsets {
    init(_ insets: EdgeInsets, _ direction: LayoutDirection) {
        switch direction {
        case .leftToRight:
            self.init(top: insets.top, left: insets.leading, bottom: insets.bottom, right: insets.trailing)
        case .rightToLeft:
            self.init(top: insets.top, left: insets.trailing, bottom: insets.bottom, right: insets.leading)
        @unknown default:
            self.init(top: insets.top, left: insets.leading, bottom: insets.bottom, right: insets.trailing)
        }
    }
}
