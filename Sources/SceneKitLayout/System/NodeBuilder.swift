//
//  NodeBuilder.swift
//  
//
//  Created by Jan Mazurczak on 01/03/2021.
//

@resultBuilder
public struct NodeBuilder {
    public static func buildBlock<A: Node>(_ a: A) -> A {
        a
    }
    public static func buildBlock<A: Node, B: Node>(_ a: A, _ b: B) -> some TupleNode {
        TupleNode2(a: a, b: b)
    }
    public static func buildBlock<A: Node, B: Node, C: Node>(_ a: A, _ b: B, _ c: C) -> some TupleNode {
        TupleNode3(a: a, b: b, c: c)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node>(_ a: A, _ b: B, _ c: C, _ d: D) -> some TupleNode {
        TupleNode4(a: a, b: b, c: c, d: d)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node, E: Node>(_ a: A, _ b: B, _ c: C, _ d: D, _ e: E) -> some TupleNode {
        TupleNode5(a: a, b: b, c: c, d: d, e: e)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node>(_ a: A, _ b: B, _ c: C, _ d: D, _ e: E, _ f: F) -> some TupleNode {
        TupleNode6(a: a, b: b, c: c, d: d, e: e, f: f)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node>(_ a: A, _ b: B, _ c: C, _ d: D, _ e: E, _ f: F, _ g: G) -> some TupleNode {
        TupleNode7(a: a, b: b, c: c, d: d, e: e, f: f, g: g)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node, H: Node>(_ a: A, _ b: B, _ c: C, _ d: D, _ e: E, _ f: F, _ g: G, _ h: H) -> some TupleNode {
        TupleNode8(a: a, b: b, c: c, d: d, e: e, f: f, g: g, h: h)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node, H: Node, I: Node>(_ a: A, _ b: B, _ c: C, _ d: D, _ e: E, _ f: F, _ g: G, _ h: H, _ i: I) -> some TupleNode {
        TupleNode9(a: a, b: b, c: c, d: d, e: e, f: f, g: g, h: h, i: i)
    }
    public static func buildBlock<A: Node, B: Node, C: Node, D: Node, E: Node, F: Node, G: Node, H: Node, I: Node, J: Node>(_ a: A, _ b: B, _ c: C, _ d: D, _ e: E, _ f: F, _ g: G, _ h: H, _ i: I, _ j: J) -> some TupleNode {
        TupleNode10(a: a, b: b, c: c, d: d, e: e, f: f, g: g, h: h, i: i, j: j)
    }
    public static func buildOptional<A: Node>(_ a: A?) -> OptionalNode<A> {
        if let node = a {
            return .some(node)
        } else {
            return .none
        }
    }
    public static func buildEither<First: Node, Second: Node>(first component: First) -> EitherNode<First, Second> {
        .first(component)
    }
    public static func buildEither<First: Node, Second: Node>(second component: Second) -> EitherNode<First, Second> {
        .second(component)
    }
}
