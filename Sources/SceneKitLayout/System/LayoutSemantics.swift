//
//  LayoutSemantics.swift
//  
//
//  Created by Jan Mazurczak on 02/05/2021.
//

public enum LayoutSemantics {
    case idle
    case layout(LayoutDefinition)
    case transform(LayoutTransform)
}

public extension LayoutSemantics {
    var layout: LayoutDefinition? {
        switch self {
        case .layout(let layout):
            return layout
        default:
            return nil
        }
    }
    var transform: LayoutTransform? {
        switch self {
        case .transform(let transform):
            return transform
        default:
            return nil
        }
    }
}
