//
//  ChildBranchCandidate.swift
//  
//
//  Created by Jan Mazurczak on 26/04/2021.
//

internal enum ChildBranchCandidate {
    init(branch: AnyBranch?, builder: AnyChildBranchBuilder) {
        if let branch = branch {
            self = .updating(
                .init(
                    branch: branch,
                    updateBranch: {
                        builder.update(branch)
                    }
                )
            )
        } else {
            self = .building(
                .init(
                    branch: builder.build()
                )
            )
        }
    }
    
    var branch: AnyBranch {
        switch self {
        case .building(let building): return building.branch
        case .updating(let updating): return updating.branch
        }
    }
    
    struct Building {
        let branch: AnyBranch
    }
    case building(Building)
    var building: Building? {
        switch self {
        case .building(let building): return building
        default: return nil
        }
    }
    
    struct Updating {
        let branch: AnyBranch
        let updateBranch: () -> Void
    }
    case updating(Updating)
    var updating: Updating? {
        switch self {
        case .updating(let updating): return updating
        default: return nil
        }
    }
}

/// Don't use it outside the framework.
public protocol InternalChildBranchBuilder {}

internal protocol AnyChildBranchBuilder: InternalChildBranchBuilder {
    var id: String { get }
    var layoutTransforms: [LayoutTransform] { get }
    var environmentModifiers: [EnvironmentModifier] { get }
    func build() -> AnyBranch
    func update(_ branch: AnyBranch)
    func prefixed(with ancestor: AnyChildBranchBuilder) -> AnyChildBranchBuilder
}

internal struct ChildBranchBuilder<ChildNode: Node>: AnyChildBranchBuilder {
    
    let id: String
    let node: ChildNode
    let layoutSemantics: LayoutSemantics
    let layoutTransformsPrefix: [LayoutTransform]
    let environmentModifiers: [EnvironmentModifier]
    
    var layoutTransforms: [LayoutTransform] {
        layoutTransformsPrefix + [layoutSemantics.transform].compactMap{$0}
    }
    
    init(
        node: ChildNode,
        id: String,
        layoutSemantics: LayoutSemantics,
        layoutTransformsPrefix: [LayoutTransform],
        environmentModifiers: [EnvironmentModifier]
    ) {
        self.id = id
        self.node = node
        self.layoutSemantics = layoutSemantics
        self.layoutTransformsPrefix = layoutTransformsPrefix
        self.environmentModifiers = environmentModifiers
    }
    
    func build() -> AnyBranch {
        Branch(
            id: id,
            node: node,
            layoutSemantics: layoutSemantics,
            incomingLayoutTransforms: layoutTransforms,
            environmentModifiers: environmentModifiers
        )
    }
    
    func update(_ branch: AnyBranch) {
        guard
            let branch = branch as? Branch<ChildNode>
        else {
            fatalError("Trying to update not matching branch. This is a serious mistake.")
        }
        branch.update(
            with: node,
            invalidateTree: false
        )
        branch.layoutSemantics = layoutSemantics
        branch.incomingLayoutTransforms = layoutTransformsPrefix
        branch.environmentModifiers = environmentModifiers
    }
    
    func prefixed(with ancestor: AnyChildBranchBuilder) -> AnyChildBranchBuilder {
        ChildBranchBuilder(
            node: node,
            id: ancestor.id + "." + id,
            layoutSemantics: layoutSemantics,
            layoutTransformsPrefix: ancestor.layoutTransforms + layoutTransformsPrefix,
            environmentModifiers: ancestor.environmentModifiers + environmentModifiers
        )
    }
    
}

internal extension Node {
    
    func childrenBuilders() -> [AnyChildBranchBuilder] {
        children().map {
            guard let builder = $0 as? AnyChildBranchBuilder else {
                fatalError("children() returns wrong elements. Don't attempt to provide this function in your Node. Only default behaviour is supported.")
            }
            return builder
        }
    }
    
    func children<ChildNode: Node>(_ path: KeyPath<Self, ChildNode>, _ id: String) -> [AnyChildBranchBuilder] {
        
        let childNode = self[keyPath: path]
        let layoutSemantics = childNode.layoutSemantics
        
        let child = ChildBranchBuilder(
            node: childNode,
            id: id,
            layoutSemantics: layoutSemantics,
            layoutTransformsPrefix: [],
            environmentModifiers: [
                (childNode as? AnyEnvironmentModifierNode)?.modifier
            ].compactMap{$0}
        )
        
        if // Branch is needed around this node
            !ChildNode.hasStaticBody ||
            childNode is AnyAnchorNode
        {
            return [child]
        }
        
        switch layoutSemantics {
        case .layout:
            return [child]
        case .idle, .transform:
            return childNode
                .childrenBuilders()
                .map {
                    $0.prefixed(with: child)
                }
        }
        
    }
    
}
