import XCTest
@testable import SceneKitLayout

final class SceneKitLayoutTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(1, 1)
    }
    
    func testNodeDidChangeOnEquatableNodes() {
        struct NodeA: Node, NodeWithoutBody, Equatable {
            let a: Int
            let b: Int
        }
        struct NodeB: Node, NodeWithoutBody {
            let a: Int
            let b: Int
        }
        
        let nodeA0 = NodeA(a: 1, b: 2)
        let nodeA1 = NodeA(a: 3, b: 4)
        let nodeA2 = NodeA(a: 3, b: 4)
        
        let nodeB0 = NodeB(a: 1, b: 2)
        let nodeB1 = NodeB(a: 3, b: 4)
        let nodeB2 = NodeB(a: 3, b: 4)
        
        XCTAssertFalse(nodeA1.remainsUnchanged(since: nodeA0))
        XCTAssertTrue(nodeA2.remainsUnchanged(since: nodeA1))
        
        XCTAssertFalse(nodeB1.remainsUnchanged(since: nodeB0))
        XCTAssertFalse(nodeB2.remainsUnchanged(since: nodeB1)) // Not equatable node will always return false
    }
    
    func testNodeDidChangeOnEquatableTupleNodes() {
        struct NodeA: Node, NodeWithoutBody, Equatable {
            let a: Int
            let b: Int
        }
        struct NodeB: Node, NodeWithoutBody {
            let a: Int
            let b: Int
        }
        struct NodeC: Node, NodeWithoutBody, Equatable {
            let a: Int
            let b: Int
        }
        
        let nodeA0 = NodeA(a: 1, b: 2)
        
        let nodeB0 = NodeB(a: 1, b: 2)
        let nodeB1 = NodeB(a: 3, b: 4)
        let nodeB2 = NodeB(a: 3, b: 4)
        
        let nodeC0 = NodeC(a: 1, b: 2)
        let nodeC1 = NodeC(a: 3, b: 4)
        let nodeC2 = NodeC(a: 3, b: 4)
        
        let tupleA0C0 = TupleNode2(a: nodeA0, b: nodeC0)
        let tupleA0C1 = TupleNode2(a: nodeA0, b: nodeC1)
        let tupleA0C2 = TupleNode2(a: nodeA0, b: nodeC2)
        
        let tupleA0B0 = TupleNode2(a: nodeA0, b: nodeB0)
        let tupleA0B1 = TupleNode2(a: nodeA0, b: nodeB1)
        let tupleA0B2 = TupleNode2(a: nodeA0, b: nodeB2)
        
        XCTAssertFalse(tupleA0C0.remainsUnchanged(since: tupleA0C1))
        XCTAssertTrue(tupleA0C1.remainsUnchanged(since: tupleA0C2))
        
        XCTAssertFalse(tupleA0B0.remainsUnchanged(since: tupleA0B1))
        XCTAssertFalse(tupleA0B1.remainsUnchanged(since: tupleA0B2)) // Not equatable node will always return false
    }
    
    func testNodeDidChangeOnEquatableOptionalNodes() {
        struct NodeA: Node, NodeWithoutBody, Equatable {
            let a: Int
            let b: Int
        }
        struct NodeB: Node, NodeWithoutBody {
            let a: Int
            let b: Int
        }
        struct NodeC: Node, NodeWithoutBody, Equatable {
            let a: Int
            let b: Int
        }
        
        let nodeA0 = NodeA(a: 1, b: 2)
        let nodeA1 = NodeA(a: 3, b: 4)
        let nodeA2 = NodeA(a: 3, b: 4)
        
        let eitherfirstA0 = EitherNode<NodeA, NodeA>.first(nodeA0)
        let eitherfirstA1 = EitherNode<NodeA, NodeA>.first(nodeA1)
        let eithersecondA2 = EitherNode<NodeA, NodeA>.second(nodeA2)
        let eitherfirstA2 = EitherNode<NodeA, NodeA>.first(nodeA2)
        
        let optionalNone = OptionalNode<NodeA>.none
        let optionalSomeA0 = OptionalNode<NodeA>.some(nodeA0)
        let optionalSomeA1 = OptionalNode<NodeA>.some(nodeA1)
        let optionalSomeA2 = OptionalNode<NodeA>.some(nodeA2)
        
        XCTAssertFalse(eitherfirstA1.remainsUnchanged(since: eitherfirstA0))
        XCTAssertFalse(eithersecondA2.remainsUnchanged(since: eitherfirstA1))
        XCTAssertTrue(eitherfirstA2.remainsUnchanged(since: eitherfirstA1))
        
        XCTAssertFalse(optionalSomeA1.remainsUnchanged(since: optionalSomeA0))
        XCTAssertFalse(optionalSomeA1.remainsUnchanged(since: optionalNone))
        XCTAssertTrue(optionalSomeA2.remainsUnchanged(since: optionalSomeA1))
        XCTAssertFalse(optionalSomeA2.remainsUnchanged(since: optionalNone))
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
