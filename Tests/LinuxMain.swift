import XCTest

import SceneKitLayoutTests

var tests = [XCTestCaseEntry]()
tests += SceneKitLayoutTests.allTests()
XCTMain(tests)
